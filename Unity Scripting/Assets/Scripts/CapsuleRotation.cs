using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotation;  //posibles ejes de rotación
    public float speed2; //velocidad de la rotación

    // Update is called once per frame
    void Update()
    {
        //Acotación de los valores de rotación al valor unitario [1,-1]
        rotation = ClampVector3(rotation);

        //desplazamiento del componente transform en base al tiempo
        transform.Rotate(rotation * (speed2 * Time.deltaTime));
    }

    //Función para acotar los valores del Vector3 entre -1 y 1

    public static Vector3 ClampVector3(Vector3 target)
    {
        float clampedX = Mathf.Clamp(target.x, -1f, 1f);
        float clampedY = Mathf.Clamp(target.y, -1f, 1f);
        float clampedZ = Mathf.Clamp(target.z, -1f, 1f);

        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);

        return result;
    }

}
